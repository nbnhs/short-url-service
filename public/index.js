const CORS_PROXY = "https://floating-chamber-82082.herokuapp.com";
const SHORT_ASSOCIATION_LIST_URL = `${CORS_PROXY}/https://myl0g.keybase.pub/nhs/shorturls.json`;

async function getRedirectURL(request) {
    const resp = await fetch(SHORT_ASSOCIATION_LIST_URL);
    const associations = await resp.json();

    console.log(associations);
    return associations[`#${request}`] || "https://en.wikipedia.org/wiki/HTTP_404";
};

console.log(window.location.href.split("/#")[1])
getRedirectURL(window.location.href.split("/#")[1]).then((url) => {
    window.location.href = url;
});
